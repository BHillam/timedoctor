#include <stdio.h>

#include "TimeDoctor.h"



char td_buff[TIMEDOCTOR_BUFFER_SIZE];
int td_idx;



#define PROLOG0()	uint32_t prim = __get_PRIMASK(); __set_PRIMASK(1); {
#define PROLOG()	uint32_t prim = __get_PRIMASK(); __set_PRIMASK(1); if(td_idx < sizeof(td_buff)-64) {
#define EPILOG()	} __set_PRIMASK(prim);




void TimeDoctor_START()
{
	PROLOG();

	td_idx += sprintf( td_buff+td_idx, "SPEED %lu\r\n", TD_GET_TICK_FREQ() );
	td_idx += sprintf( td_buff+td_idx, "MEMSPEED %lu\r\n", TD_GET_TICK_FREQ() );
	td_idx += sprintf( td_buff+td_idx, "TIME %lu\r\n", TD_GET_TICK_FREQ() );

	TD_INIT_TICK();

	EPILOG();
}

void TimeDoctor_STOP()
{
	PROLOG0();

	EPILOG();
}

void TimeDoctor_SAVE( int(*putchar_function)(int) )
{

	for( int i=0; i<td_idx; ++i ) {
		putchar_function(td_buff[i]);
	}
	putchar_function('E');
	putchar_function('N');
	putchar_function('D');
	putchar_function('\n');
}



////////////////////// User Agent //////////////////////////

void TimeDoctor_UserAgentCreate ( uint32_t uxUserAgentNumber, char *name)
{
	PROLOG();

//	td_idx += sprintf( td_buff+td_idx, "CRE 8 %lu %lu\r\n", uxUserAgentNumber, TD_GET_TICK() );
	td_idx += sprintf( td_buff+td_idx, "NAM 8 %lu %s\r\n", uxUserAgentNumber, name );

	EPILOG();
}

void TimeDoctor_UserAgentBegin( uint32_t uxUserAgentNumber )
{
	PROLOG();

	td_idx += sprintf( td_buff+td_idx, "STA 8 %lu %lu\r\n", uxUserAgentNumber, TD_GET_TICK() );

	EPILOG();
}

void TimeDoctor_UserAgentEnd( uint32_t uxUserAgentNumber )
{
	PROLOG();

	td_idx += sprintf( td_buff+td_idx, "STO 8 %lu %lu\r\n", uxUserAgentNumber, TD_GET_TICK() );

	EPILOG();
}




void TimeDoctor_FlagCreate( uint32_t uxFlagNumber, char *name)
{
	PROLOG();

//	td_idx += sprintf( td_buff+td_idx, "CRE 7 %lu %lu\r\n", uxFlagNumber, TD_GET_TICK() );
	td_idx += sprintf( td_buff+td_idx, "NAM 7 %lu %s\r\n", uxFlagNumber, name );

	EPILOG();
}

void TimeDoctor_Flag( uint32_t uxFlagNumber )
{
	PROLOG();

	td_idx += sprintf( td_buff+td_idx, "OCC 7 %lu %lu\r\n", uxFlagNumber, TD_GET_TICK() );

	EPILOG();
}


void TimeDoctor_AddString( char *msg )
{
	//TODO: make this atomic to the Flag / UserAgent
	PROLOG();

	td_idx += sprintf( td_buff+td_idx, "DSC 0 0 %s\r\n", msg );

	EPILOG();
}

void TimeDoctor_AddNumber( int number )
{
	//TODO: make this atomic to the Flag / UserAgent
	PROLOG();

	td_idx += sprintf( td_buff+td_idx, "DSC 1 1 %i\r\n", number );

	EPILOG();
}

void TimeDoctor_AddColor( int color )
{
	//TODO: make this atomic to the Flag / UserAgent
	PROLOG();

	td_idx += sprintf( td_buff+td_idx, "DSC 3 3 %i\r\n", color );

	EPILOG();
}


/////////////////////////////////////////////////////////////////////////////////////////////////


void TimeDoctor_HandlerCreate( uint32_t irq, char *name )
{
	td_idx += sprintf( td_buff+td_idx, "NAM 1 %lu %s\r\n", irq, name );
}

void TimeDoctor_HandlerEnter()
{
	PROLOG();

	uint32_t irq = 0x1FF & __get_IPSR();

	td_idx += sprintf( td_buff+td_idx, "STA 1 %lu %lu\r\n", irq, TD_GET_TICK() );

	EPILOG();
}

void TimeDoctor_HandlerExit( uint32_t *addr )
{
	PROLOG();

	uint32_t irq = 0x1FF & __get_IPSR();

	td_idx += sprintf( td_buff+td_idx, "STO 1 %lu %lu\r\n", irq, TD_GET_TICK() );

	EPILOG();
}
