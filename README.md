# TimeDoctor

A STM32ARM Cortex-M4 (and similar) frontend to the TimeDoctor real-time trace inspector https://sourceforge.net/projects/timedoctor/

![IMAGE_DESCRIPTION](TimeDoctor5.png)

How it works
==
TimeDoctor keeps a buffer in RAM and traces timestamped events. When using FreeRTOS, task switches and queue operations are traced automatically via FreeRTOS's trace facility.
Moreover, user defined events can be added to the trace for marking blocks of code of your choice.

* Tracing starts when **TimeDoctor_START()** is called. Place this call immediately before FreeRTOS objects are created to trace their creation
* Tracing stops when **TimeDoctor_STOP()** is called or the buffer is full (todo: ring buffer)
* **TimeDoctor_SAVE()** outputs the buffer to a peripheral of your choice by using a user supplied callback function

The trace is in ASCII format and can be viewed by the original TimeDoctor viewer Java app from https://sourceforge.net/projects/timedoctor/ or a newer Qt and C++ based viewer: https://github.com/theDTV2/TDV2. 

The sample project shows how to use TimeDoctor on a STM32F746-DISCO board. The STOP function is mapped to the USER_BUTTON and the SAVE function puts the buffer content to the VCP UART. 


How to use
==

- Copy the TimeDoctor* files from `Core/Inc` and `Core/Src` to your project.
- When using FreeRTOS (main use case), include `TimeDoctor_FreeRTOS.h` in `Core/Inc/FreeRTOSConfig.h` and define (set) `configUSE_TRACE_FACILITY` to 1.
- Place calls to TimeDoctor_START, TimeDoctor_STOP and finally TimeDoctor_SAVE to your code.

TimeDoctor_config.h
--
### Time Base

Config the time base that TimeDoctor is using. Options:

* **TD_USE_CYCLECOUNTER**: the cycle exact counter found in most Cortex-M MCUs except M0, M0+
* **TD_USE_TIMER**: a user supplied timer counter (not implemented, roll your own)

### Buffer Size

TimeDoctor uses a global variable for storing tracing information. The size of that buffer must be configured in the **TIMEDOCTOR_BUFFER_SIZE** definition.


TimeDoctor.h
--
TimeDoctor API functions for instrumenting the code.

### Mandatory Functions

**TimeDoctor_START**, **TimeDoctor_STOP**, and **TimeDoctor_SAVE** are mandatory and must be called from your code in that order.
TimeDoctor_SAVE needs a **putchar-like callback function** which must be implemented in your code to save the buffer content to some external peripheral like UART.

### Optional Functions

The **TimeDoctor_UserAgent*** functions are optionally used to mark blocks of your code which you want to trace in TimeDoctor. See `Core/Src/main.c` for examples.

The **TimeDoctor_Flag*** functions are optionally used to mark points in your code which you want to trace in TimeDoctor. See `Core/Src/main.c` for examples.

UserAgents and Flags can be decorated with additional strings, numbers and colors, by using the  **TimeDoctor_Add*** API functions.

The **TimeDoctor_Handler*** functions are optionally used to mark entry and exit of interrupt handlers which you want to trace in TimeDoctor. See `Core/Src/stm32f7xx_it.c` for examples.


TimeDoctor_FreeRTOS.h 
--

These functions will be automatically called when FreeRTOS's trace facility is active. At the moment FreeRTOS **tasks** and **queues** will be instrumented.
